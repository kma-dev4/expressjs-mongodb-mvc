// load passport module
const passport = require('passport');

// load passport configuration
require('../../config/passport')(passport);

module.exports = (req, res, next) => {
	passport.authenticate('jwt', function(info, user, err) {
		if (err) return res.fail(err.message);
		next();
	})(req, res, next);
};