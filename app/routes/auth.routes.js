// load express module
const express = require('express');


module.exports = (app) => {

    /**
     * Instance Declaration and initialization
     * - controller instance
     * - router instance
     * - auth middleware registration (optional)
     */

    // create user controller instance
    const users = require('../controllers/user.controller.js');

    // create route instance
    var router = express.Router()

    // create schema validator object
    const SchemaValidator = require('../middleware/SchemaValidator');
    const validateRequest = SchemaValidator();

    // Register authentication middleware
    const isAuth = require('../middleware/auth.middleware');


    /**
     * Router Registration
     * - Register router for the current route file.
     */

    // Register Route
    router.post('/register', validateRequest, users.register);

    // Login Route
    router.post('/login', validateRequest, users.login);

    // Get Auth User
    router.get('/user', validateRequest, isAuth, users.user);


    /**
     * Register all routers with application
     * - under this router all routes prefix is "/auth"
     * - change in this prefix will effect to all routes available under this route
     */
    app.use('/auth', router)
}