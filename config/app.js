require('dotenv').config()

module.exports = {
    'key': process.env.APP_KEY,
    'database': process.env.DATABASE
}