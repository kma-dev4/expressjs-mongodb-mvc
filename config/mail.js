require('dotenv').config()

module.exports = {
	host: process.env.MAIL_HOST,
	port: process.env.MAIL_PORT,
	secureConnection: process.env.MAIL_SECURE,
	auth: {
		user: process.env.MAIL_USERNAME,
		pass: process.env.MAIL_PASSWORD
	},
	tls:{
        ciphers:'SSLv3'
    }
}
